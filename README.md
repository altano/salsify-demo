# Product Filtering Condition Editor UI
A Coding Exercise for UI Developers

# Introduction

Many capabilites of Salsify are built around filtered sets of products. Products at Salsify consist of properties and their values. Properties have a datatype.

In order to create filtered sets of products in Salsify we created a condition editor. This editor is used to build a filter that Salsify applies to the full set of products. The resulting set of products, presented in a product index view, is updated as filters are added or changed.

In order to create a filter condition a user must choose a property, an operation, and one or more values. Due to the differences in property datatypes, not all operations apply to all properties.

To complete this exercise please build a user interface to create a filter and update a list of products to reflect the results. Use the exercise to demonstrate not only a solution to the problem but your approach to software design and testing.

Provide us with an archive containing the results of your work and a README file with a guided tour of your work, notes on your development process, how long you spent on the exercise, what assumptions you made, etc.  If you wish, this may also be presented as a live site.  In that case simply provide a link to the site and the README file mentioned above.

# Specification

This repository contains a mock `datastore` which includes sample products, property definitions including data types, and the complete set of filter operations. Using this datastore please create a web user inteface with the following behavior:

* A user can create a single filter
* Filters have the form `[property name] [operator] [property value]`
* Creating or updating a filter causes the the view of products to update

# Recommendations / Non-requirements
We recommend splitting the UI for this project into 2 views; one for the condition editor and one for the products list. The products list view in Salsify is reused in many places throughout the application both with and without the condition editor. For the purpose of this exercise **it is not necessary to properly filter the products**. Simply demonstrating that the product list changes and that it has knowledge of the current filter is sufficient. We are mainly interested in seeing how you design the condition editor and the interface between the views.

## Properties Types/Operators

Operators define the relationship between properties and property values. Certain operators are only valid for certain property types, operator behavior and valid operators for each property type are defined as follows:

| Operator | Description |
-----------|--------------
| Equals   | Value exactly matches |
| Is greater than | Value is greater than |
| Is less than  | Value is less than |
| Has Any Value | Value is present |
| Has No Value  | Value is absent  |
| Is Any of     | Value exactly matches one of several values |


| Property Type | Valid Operators |
---------------- | ----------------
| string | equals |
| | any |
| | none |
| | in |
| number | equals |
| | greater_than |
| | less_than |
| | any |
| | none |
| | in |
| enumerated | equals |
| | any |
| | none |
| | in |
