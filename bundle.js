define("Product", ["require", "exports"], function (require, exports) {
    "use strict";
    var Product = (function () {
        function Product(id, name, color, weight, category, isWireless, properties) {
            this.id = id;
            this.name = name;
            this.color = color;
            this.weight = weight;
            this.category = category;
            this.isWireless = isWireless;
            this.properties = properties;
        }
        return Product;
    }());
    exports.Product = Product;
});
/**
 * Initializes several collections based on data contained in datastore.js
 */
define("data", ["require", "exports", "Product"], function (require, exports, Product_1) {
    "use strict";
    exports.products = datastore.getProducts()
        .map(function (datastoreProduct) {
        var name, color, weight, category, isWireless;
        datastoreProduct.properties.forEach(function (property) {
            switch (property.name) {
                case 'Product Name':
                    name = property.value;
                    break;
                case 'color':
                    color = property.value;
                    break;
                case 'weight (oz)':
                    weight = property.value;
                    break;
                case 'category':
                    category = property.value;
                    break;
                case 'wireless':
                    isWireless = property.value === 'true';
                    break;
                default:
                    throw new Error("Unknown property: " + property.name + " = \"" + property.value + "\"");
            }
        });
        return new Product_1.Product(datastoreProduct.id, name, color, weight, category, isWireless, datastoreProduct.properties);
    });
    exports.properties = datastore.getProperties();
    exports.operators = datastore.getOperators();
    exports.typeToOperatorsMap = {
        string: exports.operators.filter(function (operator) { return ['equals', 'any', 'none', 'in'].indexOf(operator.id) !== -1; }),
        number: exports.operators.filter(function (operator) { return ['equals', 'greater_than', 'less_than', 'any', 'none', 'in'].indexOf(operator.id) !== -1; }),
        enumerated: exports.operators.filter(function (operator) { return ['equals', 'any', 'none', 'in'].indexOf(operator.id) !== -1; }),
    };
    exports.operatorsThatNeedAValue = ['equals', 'greater_than', 'less_than', 'in'];
});
define("controls/Product", ["require", "exports", "knockout"], function (require, exports, ko) {
    "use strict";
    var ProductViewModel = (function () {
        function ProductViewModel(params) {
            var _this = this;
            this.product = ko.observable();
            this.product(params.product);
            this.displayText = ko.computed(function () { return _this.product().name; });
        }
        return ProductViewModel;
    }());
    ko.components.register('product', {
        viewModel: function (params) { return new ProductViewModel(params); },
        template: "<span data-bind=\"text: displayText()\"></span>"
    });
});
define("ProductFilter", ["require", "exports"], function (require, exports) {
    "use strict";
    var ProductFilter = (function () {
        function ProductFilter(propertyDefintion, operator, propertyValue) {
            this.propertyDefintion = propertyDefintion;
            this.operator = operator;
            this.propertyValue = propertyValue;
        }
        return ProductFilter;
    }());
    exports.ProductFilter = ProductFilter;
});
define("controls/ProductListing", ["require", "exports", "knockout", "data"], function (require, exports, ko, data) {
    "use strict";
    var ProductListingViewModel = (function () {
        function ProductListingViewModel(params) {
            var _this = this;
            this.filter = ko.observable();
            this.products = ko.computed(function () {
                var products = data.products;
                if (_this.filter()) {
                    products = _this.filterProducts(products, _this.filter());
                }
                return products;
            });
            this.isFilterable = ko.observable(false);
            this.isFilterable(Boolean(params.isFilterable));
        }
        ProductListingViewModel.prototype.filterProducts = function (products, filter) {
            return products.filter(function (product) {
                var propertyBeingFiltered = product.properties.filter(function (property) {
                    return property.name === filter.propertyDefintion.name;
                })[0];
                if (!propertyBeingFiltered) {
                    var missingPropertyIsAMatch = filter.operator.id === "none";
                    return missingPropertyIsAMatch;
                }
                switch (filter.operator.id) {
                    case 'equals':
                        return propertyBeingFiltered.value == filter.propertyValue;
                    case 'greater_than':
                        return propertyBeingFiltered.value > filter.propertyValue;
                    case 'less_than':
                        return propertyBeingFiltered.value < filter.propertyValue;
                    case 'any':
                        return typeof propertyBeingFiltered !== undefined;
                    case 'none':
                        return typeof propertyBeingFiltered === undefined;
                    case 'in':
                        var values = filter.propertyValue.split(",").map(function (value) { return value.trim(); });
                        return values.some(function (value) { return (propertyBeingFiltered.value == value); });
                }
            });
        };
        return ProductListingViewModel;
    }());
    ko.components.register('product-listing', {
        viewModel: function (params) { return new ProductListingViewModel(params); },
        template: "<h1>Product Listing (<span data-bind=\"text: products().length\"></span>)</h1>\n               <product-listing-filter data-bind=\"visible: isFilterable()\" params=\"filter: filter\"></product-listing-filter>\n               <ul data-bind=\"foreach: { data: products, as: 'product' }\">\n                   <li><product params=\"product: product\" /></li>\n               </ul>"
    });
});
define("controls/ProductListingFilter", ["require", "exports", "knockout", "data", "ProductFilter"], function (require, exports, ko, data, ProductFilter_1) {
    "use strict";
    var ProductListingFilterViewModel = (function () {
        function ProductListingFilterViewModel(params) {
            var _this = this;
            this.properties = data.properties;
            this.selectedPropertyDefinition = ko.observable();
            this.selectedOperator = ko.observable();
            this.propertyValue = ko.observable();
            this.operators = ko.computed(function () {
                if (_this.selectedPropertyDefinition()) {
                    var type = _this.selectedPropertyDefinition().type;
                    if (!(type in data.typeToOperatorsMap)) {
                        throw new Error("Unknown property type");
                    }
                    else {
                        return data.typeToOperatorsMap[type];
                    }
                }
            });
            this.generatedFilter = ko.computed(function () {
                var setFilter = false;
                if (_this.selectedPropertyDefinition() && _this.selectedOperator()) {
                    if (_this.isPropertyValueVisible()) {
                        if (typeof _this.propertyValue() !== "undefined" && _this.propertyValue() !== "") {
                            setFilter = true;
                        }
                    }
                    else {
                        setFilter = true;
                    }
                }
                if (setFilter) {
                    return new ProductFilter_1.ProductFilter(_this.selectedPropertyDefinition(), _this.selectedOperator(), _this.propertyValue());
                }
            });
            this.isPropertyValueVisible = ko.computed(function () {
                if (_this.selectedOperator()) {
                    return data.operatorsThatNeedAValue.indexOf(_this.selectedOperator().id) !== -1;
                }
            });
            this.filter = params.filter;
            // Synchronize the filter generated in this control with the passed in observable
            this.generatedFilter.subscribe(function (filter) { return _this.filter(filter); });
        }
        return ProductListingFilterViewModel;
    }());
    ko.components.register('product-listing-filter', {
        viewModel: function (params) { return new ProductListingFilterViewModel(params); },
        template: "<select data-bind=\"options: properties,\n                                  optionsText: 'name',\n                                  value: selectedPropertyDefinition\"></select>\n               <select data-bind=\"options: operators,\n                                  optionsText: 'text',\n                                  value: selectedOperator\"></select>\n               <input data-bind=\"textInput: propertyValue,\n                                 visible: isPropertyValueVisible\" />"
    });
});
define("main", ["require", "exports", "knockout", "controls/Product", "controls/ProductListing", "controls/ProductListingFilter"], function (require, exports, ko) {
    "use strict";
    function run() {
        ko.applyBindings(undefined);
    }
    exports.run = run;
});
