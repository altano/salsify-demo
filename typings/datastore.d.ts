interface DatastoreProperty {
    name: string;
    value: string | number;
}

interface DatastoreProduct {
    id: number;
    properties: DatastoreProperty[];
}

interface DatastorePropertyDefinition {
    id: number;
    name: string;
    type: 'string' | 'number' | 'enumerated';
    values?: string[];
}

interface DatastoreOperator {
    text: string;
    id: string;
}

interface Datastore {
    getProducts: () => DatastoreProduct[];
    getProperties: () => DatastorePropertyDefinition[];
    getOperators: () => DatastoreOperator[];
}

declare var datastore: Datastore;