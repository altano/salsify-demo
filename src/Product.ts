export type ProductCategory = string;

export class Product {
    constructor(public id: number,
                public name: string,
                public color: string,
                public weight: number,
                public category: ProductCategory,
                public isWireless: boolean,
                public properties: DatastoreProperty[]) {

    }
}