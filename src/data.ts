/**
 * Initializes several collections based on data contained in datastore.js
 */

import { Product } from "./Product";

export var products: Product[] = datastore.getProducts()
  .map(datastoreProduct => {
    let name: string, color: string, weight: number, category: string, isWireless: boolean;

    datastoreProduct.properties.forEach(property => {
      switch (property.name) {
        case 'Product Name':
          name = <string>property.value;
          break;
        case 'color':
          color = <string>property.value;
          break;
        case 'weight (oz)':
          weight = <number>property.value;
          break;
        case 'category':
          category = <string>property.value;
          break;
        case 'wireless':
          isWireless = <string>property.value === 'true';
          break;
          default:
            throw new Error(`Unknown property: ${property.name} = "${property.value}"`);
      }
    });

    return new Product(datastoreProduct.id, name, color, weight, category, isWireless, datastoreProduct.properties);
  });

export var properties: DatastorePropertyDefinition[] = datastore.getProperties();

export var operators: DatastoreOperator[] = datastore.getOperators();

export let typeToOperatorsMap: {[index: string]: DatastoreOperator[]} = {
    string: operators.filter(operator => ['equals', 'any', 'none', 'in'].indexOf(operator.id) !== -1),
    number: operators.filter(operator => ['equals', 'greater_than', 'less_than', 'any', 'none', 'in'].indexOf(operator.id) !== -1),
    enumerated: operators.filter(operator => ['equals', 'any', 'none', 'in'].indexOf(operator.id) !== -1),
};

export let operatorsThatNeedAValue = ['equals', 'greater_than', 'less_than', 'in'];