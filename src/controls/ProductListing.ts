import ko = require("knockout");
import * as data from "src/data";
import { Product } from "src/Product";
import { ProductFilter } from "src/ProductFilter";

interface ProductListingViewModelOptions {
    isFilterable: boolean;
}

class ProductListingViewModel {
    filter = ko.observable<ProductFilter>();

    products = ko.computed<Product[]>(() => {
        let products = data.products;

        if (this.filter()) {
            products = this.filterProducts(products, this.filter());
        }

        return products;
    });

    isFilterable = ko.observable(false);

    constructor(params: ProductListingViewModelOptions) {
        this.isFilterable(Boolean(params.isFilterable));
    }

    filterProducts(products: Product[], filter: ProductFilter) {
        return products.filter(product => {
            let propertyBeingFiltered = product.properties.filter(property => {
                return property.name === filter.propertyDefintion.name;
            })[0];

            if (!propertyBeingFiltered) {
                let missingPropertyIsAMatch = filter.operator.id === "none";
                return missingPropertyIsAMatch;
            }

            switch(filter.operator.id) {
                case 'equals':
                    return propertyBeingFiltered.value == filter.propertyValue;
                case 'greater_than':
                    return propertyBeingFiltered.value > filter.propertyValue;
                case 'less_than':
                    return propertyBeingFiltered.value < filter.propertyValue;
                case 'any':
                    return typeof propertyBeingFiltered !== undefined;
                case 'none':
                    return typeof propertyBeingFiltered === undefined;
                case 'in':
                    let values = filter.propertyValue.split(",").map(value => value.trim());
                    return values.some(value => (propertyBeingFiltered.value == value));
            }

        });
    }
}

ko.components.register('product-listing', {
    viewModel: (params: ProductListingViewModelOptions) => new ProductListingViewModel(params),
    template: `<h1>Product Listing (<span data-bind="text: products().length"></span>)</h1>
               <product-listing-filter data-bind="visible: isFilterable()" params="filter: filter"></product-listing-filter>
               <ul data-bind="foreach: { data: products, as: 'product' }">
                   <li><product params="product: product" /></li>
               </ul>`
});