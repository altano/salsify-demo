import ko = require("knockout");
import * as data from "src/data";
import { Product } from "src/Product";
import { ProductFilter } from "src/ProductFilter";

interface ProductListingFilterViewModelOptions {
    filter: ko.Observable<ProductFilter>;
}

class ProductListingFilterViewModel {
    filter: ko.Observable<ProductFilter>;

    properties = data.properties;
    selectedPropertyDefinition = ko.observable<DatastorePropertyDefinition>();
    selectedOperator = ko.observable<DatastoreOperator>();
    propertyValue = ko.observable<string>();

    operators = ko.computed<DatastoreOperator[]>(() => {
        if (this.selectedPropertyDefinition()) {
            let type = this.selectedPropertyDefinition().type;
            if (!(type in data.typeToOperatorsMap)) {
                throw new Error("Unknown property type");
            }
            else {
                return data.typeToOperatorsMap[type];
            }
        }
    });

    generatedFilter = ko.computed<ProductFilter>(() => {
        let setFilter = false;
        if (this.selectedPropertyDefinition() && this.selectedOperator()) {
            if (this.isPropertyValueVisible()) {
                if (typeof this.propertyValue() !== "undefined" && this.propertyValue() !== "") {
                    setFilter = true;
                }
            }
            else {
                setFilter = true;
            }
        }

        if (setFilter) {
            return new ProductFilter(
                this.selectedPropertyDefinition(),
                this.selectedOperator(),
                this.propertyValue()
            );
        }
    });

    isPropertyValueVisible = ko.computed<boolean>(() => {
        if (this.selectedOperator()) {
            return data.operatorsThatNeedAValue.indexOf(this.selectedOperator().id) !== -1;
        }
    });

    constructor(params: ProductListingFilterViewModelOptions) {
        this.filter = params.filter;

        // Synchronize the filter generated in this control with the passed in observable
        this.generatedFilter.subscribe(filter => this.filter(filter));
    }
}

ko.components.register('product-listing-filter', {
    viewModel: (params: ProductListingFilterViewModelOptions) => new ProductListingFilterViewModel(params),
    template: `<select data-bind="options: properties,
                                  optionsText: 'name',
                                  value: selectedPropertyDefinition"></select>
               <select data-bind="options: operators,
                                  optionsText: 'text',
                                  value: selectedOperator"></select>
               <input data-bind="textInput: propertyValue,
                                 visible: isPropertyValueVisible" />`
});