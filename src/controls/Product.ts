import ko = require("knockout");
import * as data from "src/data";
import { Product } from "src/Product";

interface ProductViewModelOptions {
    product: Product;
}

class ProductViewModel {
    public product = ko.observable<Product>();

    public displayText: ko.Computed<string>;

    constructor(params: ProductViewModelOptions) {
        this.product(params.product);
        this.displayText = ko.computed(() => this.product().name);
    }
}

ko.components.register('product', {
    viewModel: (params: ProductViewModelOptions) => new ProductViewModel(params),
    template: `<span data-bind="text: displayText()"></span>`
});


