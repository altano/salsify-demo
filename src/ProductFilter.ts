export class ProductFilter {
    constructor(public propertyDefintion: DatastorePropertyDefinition,
                public operator: DatastoreOperator,
                public propertyValue?: string) {}
}