import ko = require("knockout");
import * as data from "./data";

// Import all controls
import "src/controls/Product";
import "src/controls/ProductListing";
import "src/controls/ProductListingFilter";

export function run() {
    ko.applyBindings(undefined);
}